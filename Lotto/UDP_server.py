import socket
import random

numbers=random.sample(range(1,20),5)
tipps=[]

def receive_n_send_data(sock):
  data, client_addr = sock.recvfrom(1024)
  if data:
    data= data.decode()
    print('Tippek a clienstől: ' + data)
    data=data[1:]
    data=data[:-1]
    data=[int(s) for s in data.split(',')]
    tipps=data
    sub=0
    for i in range(0,len(tipps)-1):
        if tipps[i] in numbers:
            sub+=1   
    print('A nyerő számok: ' + str(numbers))
    print('A nyeremény: ' + str(sub*int(tipps[5])))
    tipps.clear()
    sock.sendto(b'OK', client_addr)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('localhost', 10000)
sock.bind(server_address)

while True:
    receive_n_send_data(sock)
    

sock.close()