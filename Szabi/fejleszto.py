import socket
import sys
import struct
import random

class SimpleTCPSelectClient:
  def __init__(self, serverAddr='localhost', serverPort=10001):
    self.setupClient(serverAddr, serverPort)
    self.packer = struct.Struct('I I')

  def setupClient(self, serverAddr, serverPort):
    server_address = (serverAddr, serverPort)

    # Create a TCP/IP socket
    self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect the socket to the port where the server is listening
    self.client.connect(server_address)
  
  def handleIncomingMessageFromRemoteServer(self):
        data = self.client.recv(1024)
        if not data :
            print('\nDisconnected from server')
            sys.exit()
        else:
            #print('hi')
            print(data.decode('utf-8'))
  
  def handleConnection(self):
    while True:
        kezd=random.randint(21,31)
        napok=random.randint(1,5)
        self.client.sendall(self.packer.pack(kezd,napok))
        self.handleIncomingMessageFromRemoteServer()


simpleTCPSelectClient = SimpleTCPSelectClient()
simpleTCPSelectClient.handleConnection()