import socket
import random

def send_n_receive_values(server_address,  connection):
    #msg = input('5 számjegy majd a feltett tét: ')
    msg = random.sample(range(1,20),5)
    msg.append(random.randint(0,10000))
    msg=str(msg)
    print(msg)
    if msg != '':
        msg = msg.strip()
        connection.sendto(msg.encode(), server_address)
    result, server_addr = connection.recvfrom(16)
    print(result)

connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('localhost', 10000)

send_n_receive_values(server_address,connection)

connection.close()