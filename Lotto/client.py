import socket
import sys
import random
import struct

class SimpleTCPSelectClient:
  def __init__(self, serverAddr='localhost', serverPort=10001):
    self.setupClient(serverAddr, serverPort)
    self.packer = struct.Struct('I I I I I I')

  def setupClient(self, serverAddr, serverPort):
    server_address = (serverAddr, serverPort)

    # Create a TCP/IP socket
    self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect the socket to the port where the server is listening
    self.client.connect(server_address)
  
  def handleIncomingMessageFromRemoteServer(self):
        data = self.client.recv(4096).decode('utf-8')
        if not data :
            print('\nDisconnected from server')
            sys.exit()
        #elif data=='OK': # ez csak hogy utána kilépjen
        #    sys.exit()
        else:
            print(str(data))
  
  def handleConnection(self):
    while True:
        msg = input('5 számjegy majd a feltett tét: ')
        if msg != '':
            msg = msg.strip()
            self.client.sendall(msg.encode())
        self.handleIncomingMessageFromRemoteServer()

simpleTCPSelectClient = SimpleTCPSelectClient()
simpleTCPSelectClient.handleConnection()