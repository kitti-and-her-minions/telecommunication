import select
import socket
import sys
import struct
import random

class SimpleTCPSelectServer:
  def __init__(self, addr='localhost', port=10001, timeout=1):
    self.server = self.setupServer(addr, port)
    self.inputs = [ self.server ]
    self.timeout=timeout
    self.packer = struct.Struct('I I')
    self.bo=True
    self.thisdict = {
    21: 10,
    22: 6,
    23: 6,
    24: 10,
    25: 6,
    26: 6,
    27: 10,
    28: 6,
    29: 6,
    30: 10,
    31: 6
    }

  def setupServer(self, addr, port):
    # Create a TCP/IP socket
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setblocking(0)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    server_address = (addr, port)
    server.bind(server_address)
    
    server.listen(5)
    return server

  def handleNewConnection(self, sock):
    connection, client_address = sock.accept()
    connection.setblocking(0)	  
    self.inputs.append(connection)

  def handleDataFromClient(self, sock):
        data = sock.recv(self.packer.size)
        #data = data.strip()
        msg=""
        if data:
            datak, datav = self.packer.unpack(data)
            print(datak)
            print(datav)
            if(datak in self.thisdict.keys()):
                for i in range(datak,datak+datav):
                    if (i>=21) & (i<=31):
                        if self.thisdict[i] < 5 :
                            msg="ELUTASIT"
                    else:
                        msg = "ELUTASIT"
                if msg=="ELUTASIT":
                    sock.sendall("ELUTASIT".encode())
                else:
                    sock.sendall("ELFOGAD".encode())
                
            #sock.sendall(msg)
            
        else:
            print('closing ' + str(sock.getpeername()) + ' after reading no data')
            self.inputs.remove(sock)
            sock.close()

  def handleInputs(self, readable):
    for sock in readable:
        if sock is self.server:
            self.handleNewConnection(sock)
        else:
            self.handleDataFromClient(sock)

  def handleExceptionalCondition(self, exceptional):
    for sock in exceptional:
      print('handling exceptional condition for ' + str(sock.getpeername()))
      # Stop listening for input on the connection
      self.inputs.remove(sock)
      sock.close()

  def handleConnections(self):
    while self.inputs:
      try:
        readable, writable, exceptional = select.select(self.inputs, [], self.inputs, self.timeout)
    
        if not (readable or writable or exceptional):
            # timed out, do some other work here
            continue
    
        self.handleInputs(readable)
        self.handleExceptionalCondition(exceptional)
      except KeyboardInterrupt:
        print("Close the system")
        for c in self.inputs:
            c.close()
        self.inputs = []

simpleTCPSelectServer = SimpleTCPSelectServer()
simpleTCPSelectServer.handleConnections()